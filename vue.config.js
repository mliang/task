const { defineConfig } = require('@vue/cli-service')
var path = require('path')

module.exports = defineConfig({
  publicPath: './',
  transpileDependencies: true,
  pluginOptions: {
    electronBuilder: {
      customFileProtocol: "./"
    }
  }
})
