import Dexie from 'dexie';

let db;

function add(newLabel) {
  const subscribe = Dexie.liveQuery(() => db.labels.where({"name": newLabel.name}).toArray()).subscribe({
    next: label => {
      if(!label || label.length == 0){
        db.labels.add(newLabel);
      }
      subscribe.unsubscribe();
    },
    error: error => console.error(error)
  });
}

function getList() {
  return Dexie.liveQuery(() => db.labels.reverse().sortBy('id'))
}

function getInfoByName(name) {
  return Dexie.liveQuery(() => db.labels.get({ "name": name }))
}

function getInfoById(id) {
  return Dexie.liveQuery(() => db.labels.get({ "id": id }))
}

function rename(label) {
  db.labels.where({ "id": label.id }).modify({ "name": label.name })
}

function setPid(label) {
  db.labels.where({ "id": label.id }).modify({ "p_id": label.p_id })
}

function del(label) {
  db.labels.where({ "id": label.id }).delete()
}

function importData(data){
  data.forEach(ele => {
    add(ele)
  });
}


export default function(_db){
  db = _db;
  return {
    add,
    getList,
    rename,
    setPid,
    del,
    getInfoByName,
    getInfoById,
    importData
  }
} 