import Dexie from 'dexie';

let db;

function add(newMark) {
  const subscribe = Dexie.liveQuery(() => db.marks.where({"name": newMark.name}).toArray()).subscribe({
    next: marks => {
      if(!marks || marks.length == 0){
        db.marks.add(newMark);
      }
      subscribe.unsubscribe();
    },
    error: error => console.error(error)
  });
}

function rename(marks) {
  db.marks.where({ "id": marks.id }).modify({ "name": marks.name })
}

function del(marks) {
  db.marks.where({ "id": marks.id }).delete()
}

function getList() {
  return Dexie.liveQuery(() => db.marks.reverse().sortBy('id'))
}

function remark(marks) {
  db.marks.where({ "id": marks.id }).modify({ "mark": marks.mark })
}

function importData(data){
  data.forEach(ele => {
    delete ele.id
    add(ele)
  });
}

export default function(_db){
  db = _db;
  return {
    add,
    rename,
    del,
    getList,
    remark,
    importData
  }
} 