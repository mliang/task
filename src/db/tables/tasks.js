import Dexie from 'dexie';

let db;

async function add(data) {
  await db.tasks.add(data);
}

function del(task) {
  db.tasks.where({ "id": task.id }).delete()
}

function getList(classify) {
  if (classify) {
    if (classify.mode == 'is_date') {
      if (classify.start && classify.end) {
        return Dexie.liveQuery(() =>
          db.tasks.where("create_date").between(
            classify.start,
            classify.end
          ).reverse().sortBy('create_date'))
      } else if (classify.start) {
        return Dexie.liveQuery(() =>
          db.tasks.where("create_date").aboveOrEqual(
            classify.start
          ).reverse().sortBy('create_date'))
      } else if (classify.end) {
        return Dexie.liveQuery(() =>
          db.tasks.where("create_date").belowOrEqual(
            classify.end
          ).reverse().sortBy('create_date'))
      } else {
        return Dexie.liveQuery(() => db.tasks.toArray())
      }
    } else if (classify.mode == 'is_classify') {
      if (classify.type == 'un_classified') {
        return Dexie.liveQuery(() => db.tasks.reverse().filter(function (task) {
          //task.label对应label被删除
          return !task.label;
        }).sortBy('create_date'))
      } else if (classify.type == 'finished') {
        return Dexie.liveQuery(() => db.tasks.where("finish_date").notEqual("").reverse().sortBy('finish_date'))
      } else if (classify.type == 'un_finished') {
        return Dexie.liveQuery(() => db.tasks.where("finish_date").equals("").reverse().sortBy('finish_date'))
      } else {
        return Dexie.liveQuery(() => db.tasks.toArray())
      }
    } else if (classify.mode == 'is_label') {
      return Dexie.liveQuery(() => db.tasks.where({ "label": classify.type }).reverse().sortBy('create_date'))
    } else {
      return Dexie.liveQuery(() => db.tasks.toArray())
    }
  } else {
    return Dexie.liveQuery(() => db.tasks.toArray())
  }
}

function getUnFinishedNums(){
  return Dexie.liveQuery(() => db.tasks.where("finish_date").equals("").reverse().count())
}

function finish(task) {
  db.tasks.where({ "id": task.id }).modify({ "finish_date": task.finish_date })
}

function rename(task) {
  db.tasks.where({ "id": task.id }).modify({ "name": task.name })
}

function remark(task) {
  db.tasks.where({ "id": task.id }).modify({ "remark": task.remark })
}

function relabel(task) {
  db.tasks.where({ "id": task.id }).modify({ "label": task.label })
}

function delByLabel(label) {
  db.tasks.where({ "label": label.id }).modify({ "label": null })
}

function importData(data){
  data.forEach(ele => {
    delete ele.id
    add(ele)
  });
}

export default function (_db) {
  db = _db;
  return {
    add,
    relabel,
    getList,
    finish,
    rename,
    remark,
    del,
    delByLabel,
    getUnFinishedNums,
    importData
  }
}