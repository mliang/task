import Dexie from 'dexie';
let db;

async function add(config) {
  await db.config.add(config)
}

function getConfig() {
  return Dexie.liveQuery(() => db.config.toArray())
}

function changeLabel(label) {
  const subscribe = Dexie.liveQuery(() => db.config.toArray()).subscribe({
    next: configs => {
      if (configs && configs.length > 0) {
        db.config.where({ "id": configs[0].id }).modify({ "label": label })
      } else {
        add({ "label": label })
      }
      subscribe.unsubscribe()
    },
    error: error => console.error(error)
  })
}
function changeClassify(classify) {
  const subscribe = Dexie.liveQuery(() => db.config.toArray()).subscribe({
    next: configs => {
      if (configs && configs.length > 0) {
        db.config.where({ "id": configs[0].id }).modify({ "classify": classify })
      } else {
        add({ "classify": classify })
      }
      subscribe.unsubscribe()
    },
    error: error => console.error(error)
  })
}

function importData(data){
  data.forEach(ele => {
    add(ele)
  });
}


export default function (_db) {
  db = _db;
  return {
    changeLabel,
    getConfig,
    changeClassify,
    importData
  }
} 