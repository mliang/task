import Dexie from 'dexie';
import tasks from '@/db/tables/tasks';
import labels from '@/db/tables/labels';
import config from '@/db/tables/config';
import marks from '@/db/tables/marks';


const db = new Dexie('MyTask');

db.version(4).stores({
    labels: '++id, name, p_id',
    tasks: '++id, name, create_date, finish_date, start_date, end_date, label, remark',
    config: '++id, label, classify',
    marks: '++id, name, mark'
});

export default {
    tasks: tasks(db),
    labels: labels(db),
    config: config(db),
    marks: marks(db),
}
