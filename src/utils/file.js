import moment from "moment";

export function exportFileJson(data) {

  if (typeof data === 'object') {
    data = JSON.stringify(data, null, 4);
  }
  // 导出数据
  const blob = new Blob([data], { type: 'text/json' }),
    e = new MouseEvent('click'),
    a = document.createElement('a');

  a.download = "每日清单导出数据-" + moment().format("YYYY年MM月DD日 HH:mm:ss") + ".json";
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
  a.dispatchEvent(e);
}

export function importFileJson() {
  const input = document.createElement("input");
  input.type = "file"
  input.click();

  return new Promise((resolve, reject) => {
    input.addEventListener("change", event => {
      const reader = new FileReader()

      reader.onload = () => {
        const jsonData = JSON.parse(reader.result)
        resolve(jsonData)
      }

      reader.readAsText(event.target.files[0])

    }, { once: true });
  })
}

export default {
  exportFileJson,
  importFileJson
}