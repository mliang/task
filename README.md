# 任务清单-task

## 说明

![image](https://gitee.com/mliang/pictures/raw/master/task1.png)

### window 安装路径

> dist_electron/task Setup 0.3.0.exe

### 更新日志 3.0
1. 增加数据导出导入功能
2. 标签栏调整，且固定
3. vue升级到2.7，代码调整为组合式

### 更新日志 2.1
1. 目录树改造，增加目录移动

### 更新日志 2.0
1. 使用ElementUI替换Bootstrap3
2. 增加重要备注模块
3. 去除待整理标签

### 更新日志 1.1
1. 去除备注回车保存事件
2. 优化备注换行展示
3. 未完成标签增加任务数量标记
4. 修复上月数据丢失最后一天问题

### 更新日志 1.0 
1. 任务管理
2. 标签管理
3. 任务标签过滤

## 开发

### 安装依赖
```
yarn install
```

> vue + electron + dexie

### 开发运行
```
yarn serve
```

### 打包
```
yarn electron:build
```
> 打包后安装软件目录：dist_electron

### TODO

> 日历模块，4.0

